import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Admin } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  mail = "";
  password = "";
  constructor(private energyService: EnergyService, private router: Router) { }

  ngOnInit(): void {
  }
  adminLogin() {
    this.energyService.adminLogin({"mail": "mailtosruthiramachandran@gmail.com", "password" : "Sruthi12"}).subscribe((res) => {
      this.router.navigate(['displayadmin']);
    }, (err: any) => {
      this.router.navigate(['']);
    });
  }

}
