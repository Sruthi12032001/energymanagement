import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotEnrolledComponent } from './not-enrolled.component';

describe('NotEnrolledComponent', () => {
  let component: NotEnrolledComponent;
  let fixture: ComponentFixture<NotEnrolledComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotEnrolledComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotEnrolledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
