import { Component, OnInit } from '@angular/core';
import { SmartMeters } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-not-enrolled',
  templateUrl: './not-enrolled.component.html',
  styleUrls: ['./not-enrolled.component.css']
})
export class NotEnrolledComponent implements OnInit {

  smartMeters: SmartMeters[] = [];
  constructor(private energyService: EnergyService) { }

  ngOnInit(): void {
    this.energyService.getSmartMetersNotEnrolled().subscribe((res: any) => {
      (res.data).forEach((element: any) => {
        this.smartMeters.push(new SmartMeters(element.id, element.newlyCreated, element.allowed, element.datas, element.holder));
      });
    })
  }

  actionEnroll(i: number) {
    this.energyService.changeSmartMeterEnrollStatus(this.smartMeters[i]).subscribe((res: any) => {
      this.smartMeters = [];
      (res.data).forEach((element: any) => {
        this.smartMeters.push(new SmartMeters(element.id, element.newlyCreated, element.allowed, element.datas, element.holder));
      });
    });
  }

  actionReject( i: number) {
    this.energyService.rejectSmartMeter(this.smartMeters[i]).subscribe((res: any) => {
      this.smartMeters = [];
      (res.data).forEach((element: any) => {
        this.smartMeters.push(new SmartMeters(element.id, element.newlyCreated, element.allowed, element.datas, element.holder));
      });
    });
  }

}
