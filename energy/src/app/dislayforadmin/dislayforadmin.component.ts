import { Component, OnInit } from '@angular/core';
import { Providers } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-dislayforadmin',
  templateUrl: './dislayforadmin.component.html',
  styleUrls: ['./dislayforadmin.component.css']
})
export class DislayforadminComponent implements OnInit {
  providers : Providers[] = []; 
  enrolled !: boolean;
  constructor(private energyService: EnergyService) { }

  ngOnInit(): void {
    this.energyService.getProviders().subscribe((res: any) => {
      (res.data).forEach((element: any) => {
        this.providers.push(new Providers(element.id, element.name, element.amountPerKw, element.enabled));
      });
    })
    this.enrolled = true;
  }

  action(i: number) {
    this.energyService.changeStatus(this.providers[i]).subscribe((res: any) => {
      const element = res.data;
      this.providers[i] = new Providers(element.id, element.name, element.amountPerKw, element.enabled);
    }) 
  }

  enrollMethod(enroll: boolean) {
    this.enrolled = enroll;
  }

}
