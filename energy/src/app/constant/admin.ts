export class Admin {
    mail: string;
    password: string;
    constructor(mail: string, password: string) {
        this.mail = mail;
        this.password = password;
    }
}
export class Providers {
    id: string;
    name: string;
    amount: number;
    enabled: string;
    action: string;
    constructor(id: string, name: string, amount: number, enabled: boolean) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.action = enabled ? "disable" : "enable";
        this.enabled = enabled ? "enabled" : "disabled";
    } 
}

export class SmartMeters {
    id: string;
    datas: object;
    holder: string;
    actionAbled: string;
    enabled: string;
    created: string;
    constructor(id : string, newlyCreated: boolean, allowed: boolean, datas: object, holder: string) {
        this.id = id;
        this.holder = holder;
        this.actionAbled = allowed ? "disable" : "enable";
        this.enabled = allowed ? "enabled" : "disabled";
        this.datas = datas;
        this.created = newlyCreated ? "enroll" : "enrolled";

    } 
}
