import { Component, OnInit } from '@angular/core';
import { SmartMeters } from '../constant/admin';
import { EnergyService } from '../service/energy.service';

@Component({
  selector: 'app-enrolled',
  templateUrl: './enrolled.component.html',
  styleUrls: ['./enrolled.component.css']
})
export class EnrolledComponent implements OnInit {

  smartMeters : SmartMeters[] = []; 
  constructor(private energyService: EnergyService) { }

  ngOnInit(): void {
    this.energyService.getSmartMetersEnrolled().subscribe((res: any) => {
      (res.data).forEach((element: any) => {
        this.smartMeters.push(new SmartMeters(element.id, element.newlyCreated, element.allowed, element.datas, element.holder));
      });
    })
  }

  action(i: number) {
    this.energyService.changeSmartMeterStatus(this.smartMeters[i]).subscribe((res: any) => {
      const element = res.data;
      this.smartMeters[i] = new SmartMeters(element.id, element.newlyCreated, element.allowed, element.datas, element.holder);
    }) 
  }

}
