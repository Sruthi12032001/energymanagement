import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { DislayforadminComponent } from './dislayforadmin/dislayforadmin.component';
import { EnrolledComponent } from './enrolled/enrolled.component';
import { NotEnrolledComponent } from './not-enrolled/not-enrolled.component';
import { NewProviderComponent } from './new-provider/new-provider.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    UserComponent,
    HomeComponent,
    DislayforadminComponent,
    EnrolledComponent,
    NotEnrolledComponent,
    NewProviderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
