import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { DislayforadminComponent } from './dislayforadmin/dislayforadmin.component';
import { HomeComponent } from './home/home.component';
import { NewProviderComponent } from './new-provider/new-provider.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {path: 'admin', component: AdminComponent},
  {path: 'user', component: UserComponent},
  {path: '', component: HomeComponent},
  {path: 'displayadmin', component: DislayforadminComponent},
  {path: 'addProvider', component: NewProviderComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
