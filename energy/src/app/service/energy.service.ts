import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Admin, Providers, SmartMeters } from '../constant/admin';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EnergyService {

  constructor(private http: HttpClient) { }
  
  adminLogin(object: Admin): Observable<any> {
    return this.http.post(`${environment.baseUrl}admin/login`, object);
  }

  getProviders(): Observable<any> {
    return this.http.get(`${environment.baseUrl}providers`);
  }

  changeStatus(object: Providers): Observable<any> {
    const provider = {"_id": object.id, "name": object.name, "amount_per_kw": object.amount, "enabled": object.enabled === "disabled" ? true: false};
    return this.http.put(`${environment.baseUrl}changeStatus/${object.id}`, provider);
  }

  getSmartMetersEnrolled() : Observable<any> {
    return this.http.get(`${environment.baseUrl}smartMetersEnrolled`);
  }

  changeSmartMeterStatus(object: SmartMeters) : Observable<any> {
    const smartMeter = {"_id": object.id, "holder": object.holder, "allowed": object.enabled === "disabled" ? true: false};
    return this.http.put(`${environment.baseUrl}changeSmartMeterStatus/${object.id}`, smartMeter);
  }

  getSmartMetersNotEnrolled() : Observable<any> {
    return this.http.get(`${environment.baseUrl}smartMetersNotEnrolled`);
  }

  changeSmartMeterEnrollStatus(object: SmartMeters) : Observable<any> {
    const smartMeter = {"_id": object.id, "holder": object.holder, "allowed": object.created === "enroll" ? true: false, "newlyCreated": object.created === "enroll" ? false: true};
    return this.http.put(`${environment.baseUrl}changeSmartMeterEnrollStatus/${object.id}`, smartMeter);
  }

  rejectSmartMeter(object: SmartMeters) : Observable<any> {
    return this.http.delete(`${environment.baseUrl}deleteSmartUser/${object.id}`);
  }

  addProvider(object: any) : Observable<any> {
    return this.http.post(`${environment.baseUrl}addProvider`, object);
  }


}
