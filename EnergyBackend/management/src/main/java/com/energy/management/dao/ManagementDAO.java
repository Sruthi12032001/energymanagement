package com.energy.management.dao;

import com.energy.management.entity.Admin;
import com.energy.management.entity.Providers;
import com.energy.management.entity.SmartMeters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ManagementDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private static final Logger logger = LogManager.getLogger(ManagementDAO.class);
    public List<Admin> adminLogin(Admin admin) {
        Query query = new Query();
        query.addCriteria(Criteria.where("mail").is(admin.getMail()).andOperator(Criteria.where("password").is(admin.getPassword())));
        return mongoTemplate.find(query, Admin.class);
    }

    public List<Providers> getProviders() {
        return  mongoTemplate.findAll(Providers.class);
    }

    public Providers changeStatus(String id, Providers providers) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Providers provider = this.mongoTemplate.findOne(query, Providers.class);
        provider.setEnabled(providers.isEnabled());

        return this.mongoTemplate.save(provider);
    }

    public List<SmartMeters> getSmartMetersEnrolled() {
        Query query = new Query();
        query.addCriteria(Criteria.where("newlyCreated").is(false));
        return this.mongoTemplate.find(query, SmartMeters.class);
    }

    public SmartMeters changeSmartMeterStatus(String id, SmartMeters smartMeters) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        SmartMeters smartMeter = this.mongoTemplate.findOne(query, SmartMeters.class);
        smartMeter.setAllowed(smartMeters.isAllowed());

        return this.mongoTemplate.save(smartMeter);
    }

    public List<SmartMeters> getSmartMetersNotEnrolled() {
        Query query = new Query();
        query.addCriteria(Criteria.where("newlyCreated").is(true));
        return this.mongoTemplate.find(query, SmartMeters.class);
    }

    public List<SmartMeters> changeSmartMeterEnrollStatus(String id, SmartMeters smartMeters) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        SmartMeters smartMeter = this.mongoTemplate.findOne(query, SmartMeters.class);
        smartMeter.setNewlyCreated(smartMeters.isNewlyCreated());
        smartMeter.setAllowed(smartMeters.isAllowed());
        this.mongoTemplate.save(smartMeter);
        return getSmartMetersNotEnrolled();
    }

    public List<SmartMeters> deleteSmartMeter(String id) {
        this.mongoTemplate.remove(new Query().addCriteria(Criteria.where("id").is(id)), SmartMeters.class);
        return getSmartMetersNotEnrolled();
    }

    public Providers addProvider(Providers providers) {
        return this.mongoTemplate.save(providers);
    }
}
