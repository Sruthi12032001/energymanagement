package com.energy.management.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("smartmeterid")
public class SmartMeters {
    @Id
    private String id;
    @Field("newly_created")
    private boolean newlyCreated;
    private boolean allowed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isNewlyCreated() {
        return newlyCreated;
    }

    public void setNewlyCreated(boolean newlyCreated) {
        this.newlyCreated = newlyCreated;
    }

    public boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public Datas[] getDatas() {
        return datas;
    }

    public void setDatas(Datas[] datas) {
        this.datas = datas;
    }

    private String holder;
    private Datas[] datas;
}
