package com.energy.management.service;

import com.energy.management.dao.ManagementDAO;
import com.energy.management.entity.Admin;
import com.energy.management.entity.Providers;
import com.energy.management.entity.SmartMeters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManagementService {
    @Autowired
    private ManagementDAO managementDAO;
    private static final Logger logger = LogManager.getLogger(ManagementService.class);
    public List<Admin> adminLogin(Admin admin) {
        List<Admin> list = this.managementDAO.adminLogin(admin);
        if(list.size() > 0) {
            return list;
        } else {
            throw new RuntimeException("Enter valid mail id and password");
        }
    }

    public List<Providers> getProviders() {
        List<Providers> list = this.managementDAO.getProviders();
        if(list.size() > 0) {
            return list;
        } else {
            throw new RuntimeException("Data not found");
        }
    }

    public Providers changeStatus(String id, Providers providers) {
        return this.managementDAO.changeStatus(id, providers);
    }


    public List<SmartMeters> getSmartMetersEnrolled() {
        return this.managementDAO.getSmartMetersEnrolled();
    }


    public SmartMeters changeSmartMeterStatus(String id, SmartMeters smartMeters) {
        return this.managementDAO.changeSmartMeterStatus(id, smartMeters);
    }

    public List<SmartMeters> getSmartMetersNotEnrolled() {
        return this.managementDAO.getSmartMetersNotEnrolled();
    }

    public List<SmartMeters> changeSmartMeterEnrollStatus(String id, SmartMeters smartMeters) {
        return this.managementDAO.changeSmartMeterEnrollStatus(id, smartMeters);
    }

    public List<SmartMeters> deleteSmartMeter(String id) {
        return this.managementDAO.deleteSmartMeter(id);
    }

    public Providers addProvider(Providers providers) {
        return this.managementDAO.addProvider(providers);
    }
}
