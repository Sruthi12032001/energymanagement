package com.energy.management.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
@Document("providers")
public class Providers {
    @Id
    private String id;
    @Field("amount_per_kw")
    private float amountPerKw;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getAmountPerKw() {
        return amountPerKw;
    }

    public void setAmountPerKw(float amountPerKw) {
        this.amountPerKw = amountPerKw;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    private String name;
    private boolean enabled;

}
