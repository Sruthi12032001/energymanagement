package com.energy.management.entity;

import java.util.Date;

public class Datas {
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public double getReading() {
        return reading;
    }

    public void setReading(double reading) {
        this.reading = reading;
    }

    private Date time;
    private double reading;
}
