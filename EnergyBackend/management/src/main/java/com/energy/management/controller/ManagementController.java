package com.energy.management.controller;

import com.energy.management.Response;
import com.energy.management.entity.Providers;
import com.energy.management.entity.SmartMeters;
import com.energy.management.service.ManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.energy.management.entity.Admin;

import java.util.List;

@RestController
@CrossOrigin
public class ManagementController {
    @Autowired
    private ManagementService managementService;
    @PostMapping("/admin/login")
    public ResponseEntity<Response> adminLogin(@RequestBody Admin admin) {
        try {
            Response response = new Response();
            response.setData(this.managementService.adminLogin(admin));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(Exception e) {
            Response response = new Response();
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/providers")
    public ResponseEntity<Response> getProviders() {
        try {
            Response response = new Response();
            response.setData(this.managementService.getProviders());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Response response = new Response();
            response.setData(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/changeStatus/{id}")
    public ResponseEntity<Response> changeStatus(@RequestBody Providers providers, @PathVariable String id) {
        try {
            Response response = new Response();
            response.setData(this.managementService.changeStatus(id, providers));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Response response = new Response();
            response.setData(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/smartMetersEnrolled")
    public ResponseEntity<Response> getSmartMetersEnrolled() {
        try {
            Response response = new Response();
            response.setData(this.managementService.getSmartMetersEnrolled());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Response response = new Response();
            response.setData(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/changeSmartMeterStatus/{id}")
    public ResponseEntity<Response> changeStatus(@RequestBody SmartMeters smartMeters, @PathVariable String id) {
        System.out.println(smartMeters.isAllowed());
        try {
            Response response = new Response();
            response.setData(this.managementService.changeSmartMeterStatus(id, smartMeters));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Response response = new Response();
            response.setData(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/smartMetersNotEnrolled")
    public ResponseEntity<Response> getSmartMetersNotEnrolled() {
        try {
            Response response = new Response();
            response.setData(this.managementService.getSmartMetersNotEnrolled());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Response response = new Response();
            response.setData(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/changeSmartMeterEnrollStatus/{id}")
    public ResponseEntity<Response> changeEnrollStatus(@RequestBody SmartMeters smartMeters, @PathVariable String id) {
        try {
            Response response = new Response();
            response.setData(this.managementService.changeSmartMeterEnrollStatus(id, smartMeters));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Response response = new Response();
            response.setData(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping("/deleteSmartUser/{id}")
    public ResponseEntity<Response> deleteSmartMeter(@PathVariable String id) {
        try {
            Response response = new Response();
            response.setData(this.managementService.deleteSmartMeter(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Response response = new Response();
            response.setData(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/addProvider")
    public ResponseEntity<Response> addProvider(@RequestBody Providers providers) {
        try {
            Response response = new Response();
            response.setData(this.managementService.addProvider(providers));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Response response = new Response();
            response.setData(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
